#ifndef SETUP_H
#define SETUP_H

#include <stdint.h>

struct startup_info {
	uint32_t rd_addr;
	uint32_t rd_len;
	uint32_t rd_maxaddr;
	uint32_t setup_addr;
	uint32_t cmdline_addr;
	uint32_t reloc_size;
};

struct highmove {
	uint32_t dst;
	uint32_t src;
	uint32_t len;
};

struct highmove_info {
	uint32_t mv_entry;
	struct highmove mv[3];
};

#define LINUX_MAGIC ('H' + ('d' << 8) + ('r' << 16) + ('S' << 24))
#define OLD_CMDLINE_MAGIC 0xA33F

struct setup_header {
	uint8_t	 setup_sects;
	uint16_t root_flags;
	uint32_t syssize;
	uint16_t ram_size;
#define RAMDISK_IMAGE_START_MASK	0x07FF
#define RAMDISK_PROMPT_FLAG		0x8000
#define RAMDISK_LOAD_FLAG		0x4000
	uint16_t vid_mode;
	uint16_t root_dev;
	uint16_t boot_flag;
	uint16_t jump;
	uint32_t header;
	uint16_t version;
	uint32_t realmode_swtch;
	uint16_t start_sys;
	uint16_t kernel_version;
	uint8_t	 type_of_loader;
	uint8_t	 loadflags;
#define LOADED_HIGH	(1<<0)
#define KEEP_SEGMENTS	(1<<6)
#define CAN_USE_HEAP	(1<<7)
	uint16_t setup_move_size;
	uint32_t code32_start;
	uint32_t ramdisk_image;
	uint32_t ramdisk_size;
	uint32_t bootsect_kludge;
	uint16_t heap_end_ptr;
	uint16_t _pad1;
	uint32_t cmd_line_ptr;
	uint32_t initrd_addr_max;
	uint32_t kernel_alignment;
	uint8_t	 relocatable_kernel;
	uint8_t	 _pad2[3];
	uint32_t cmdline_size;
	uint32_t hardware_subarch;
	uint64_t hardware_subarch_data;
} __attribute__((packed));

#endif	/* SETUP_H */
