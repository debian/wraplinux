/* ----------------------------------------------------------------------- *
 *
 *   Copyright 2008 rPath, Inc. - All Rights Reserved
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 *   Boston MA 02110-1301, USA; either version 2 of the License, or
 *   (at your option) any later version; incorporated herein by reference.
 *
 * ----------------------------------------------------------------------- */

/*
 * cwrite.c
 *
 * Conditional fwrite and writezero
 */

#include "wraplinux.h"

size_t c_fwrite(const void *ptr, size_t bytes, FILE *stream)
{
	if (!stream)
		return bytes;

	return fwrite(ptr, 1, bytes, stream);
}

size_t c_writezero(size_t n, FILE *stream)
{
	static const char zerobuf[BUFSIZ]; /* All zero */
	size_t r, o = 0;

	if (!stream)
		return n;

	while (n) {
		size_t w = (n < BUFSIZ) ? n : BUFSIZ;
		r = fwrite(zerobuf, 1, w, stream);
		o += r;
		n -= r;
		if (r < w)
			break;
	}
	return o;
}
