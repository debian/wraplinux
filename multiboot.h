/* ----------------------------------------------------------------------- *
 *
 *   Copyright 2008 rPath, Inc. - All Rights Reserved
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 *   Boston MA 02110-1301, USA; either version 2 of the License, or
 *   (at your option) any later version; incorporated herein by reference.
 *
 * ----------------------------------------------------------------------- */

/*
 * multiboot.h
 *
 * Multiboot specification header
 */

#ifndef MULTIBOOT_H
#define MULTIBOOT_H

struct multiboot_header {
	uint32_t magic;
	uint32_t flags;
	uint32_t checksum;
	uint32_t header_addr;
	uint32_t load_addr;
	uint32_t load_end_addr;
	uint32_t entry_addr;
	uint32_t mode_type;
	uint32_t width;
	uint32_t height;
	uint32_t depth;
};

#define MULTIBOOT_MAGIC 0x1badb002

#define MB_FLAG_PAGE	0x00000001 /* Page align */
#define MB_FLAG_MEMINFO	0x00000002 /* Need memory information */
#define MB_FLAG_VIDINFO 0x00000004 /* Video information specified */
#define MB_FLAG_MBLOAD	0x00010000 /* Use the load info in Multiboot header */
				   /* (obligatory for non-ELF modules) */

#endif /* MULTIBOOT_H */
