/* ----------------------------------------------------------------------- *
 *
 *   Copyright 2008 rPath, Inc. - All Rights Reserved
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 *   Boston MA 02110-1301, USA; either version 2 of the License, or
 *   (at your option) any later version; incorporated herein by reference.
 *
 * ----------------------------------------------------------------------- */

/*
 * segment.h
 *
 * Descriptor for memory segments
 */

#ifndef SEGMENT_H
#define SEGMENT_H

#include <stdio.h>
#include <inttypes.h>
#include "elf32.h"		/* For constants */

typedef uint32_t addr_t;

struct segment {
	struct segment *next;
	size_t length;
	int align;		/* Alignment (if supported) as a power of 2 */
	addr_t address;
	uint32_t sh_type;	/* Uses ELF constants, in host byte order */
	uint32_t sh_flags;	/* d:o */
	const char *name;
	const void *data;
};

struct segment *sort_segments(struct segment *list);
int output_elf(struct segment *segs, addr_t entry, FILE *out);
int output_multiboot(struct segment *segs, addr_t entry, FILE *out);
int output_nbi(struct segment *segs, addr_t entry, FILE *out);

#endif /* SEGMENT_H */
