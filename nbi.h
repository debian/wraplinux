/* ----------------------------------------------------------------------- *
 *
 *   Copyright 2008 rPath, Inc. - All Rights Reserved
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 *   Boston MA 02110-1301, USA; either version 2 of the License, or
 *   (at your option) any later version; incorporated herein by reference.
 *
 * ----------------------------------------------------------------------- */

/*
 * nbi.h
 *
 * NBI format definition, written from spec
 */

#ifndef NBI_H
#define NBI_H

#include <stdint.h>

#define NBI_MAGIC 0x1b031336

struct nbi_header {
	uint32_t magic;
	uint32_t flags;
	uint16_t header_off, header_seg;
	uint32_t entry;
};

#define NBI_HFLAG_RETURN	0x00000100
#define NBI_HFLAG_PROTMODE	0x80000000

struct nbi_image_header {
	uint8_t lengths;
	uint8_t tags;
	uint8_t resv;
	uint8_t flags;
	uint32_t load_addr;
	uint32_t filesz;
	uint32_t memsz;
};

#define NBI_IFLAG_ADDR_LAST	0x01
#define NBI_IFLAG_ADDR_END	0x02
#define NBI_IFLAG_LAST		0x04

#define NBI_HEADER_SIZE		510

#endif /* NBI_H */
