Summary: Utility to wrap a Linux kernel and initrd into an ELF or NBI file
Name: wraplinux
Version: 1.7
Release: 1
License: GPL
Group: Applications/System
URL: http://www.kernel.org/pub/linux/utils/boot/wraplinux/
Source: http://www.kernel.org/pub/linux/utils/boot/wraplinux/wraplinux-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Epoch: 1

%description
A tool to wrap an x86 Linux kernel and one or more initrd files into a
single file in ELF or NBI format, as required by some booting protocols.

%prep
%setup -q 

%build
%configure
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install INSTALLROOT="$RPM_BUILD_ROOT"

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc COPYING
%{_bindir}/wraplinux
%{_mandir}/man1/wraplinux.1*

%changelog
* Thu Jan 10 2008 H. Peter Anvin <hpa@zytor.com>
- Initial version.
