/* ----------------------------------------------------------------------- *
 *
 *   Copyright 2008 rPath, Inc. - All Rights Reserved
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 *   Boston MA 02110-1301, USA; either version 2 of the License, or
 *   (at your option) any later version; incorporated herein by reference.
 *
 * ----------------------------------------------------------------------- */

#ifndef WRAPLINUX_H
#define WRAPLINUX_H

#include "config.h"

#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <sysexits.h>

#include "segment.h"

#define WRAPLINUX_PACKAGE "wraplinux"

extern const char *program;

struct opt {
	const char *params;
	int (*output) (struct segment *, addr_t, FILE *);
	bool loadhigh;
} opt;

struct string_list {
	struct string_list *next;
	const char *str;
};

/* Convenience inline functions */
static inline addr_t align_up(addr_t addr, int align)
{
	addr_t align_mask = ((addr_t)1 << align)-1;
	return (addr + align_mask) & ~align_mask;
}

static inline addr_t align_down(addr_t addr, int align)
{
	addr_t align_mask = ((addr_t)1 << align)-1;
	return addr & ~align_mask;
}

static inline addr_t padsize(addr_t addr, int align)
{
	addr_t align_mask = ((addr_t)1 << align)-1;
	return -addr & align_mask;
}

/* linux.c */
int wrap_kernel(const char *kernel, const char *cmdline,
		const struct string_list *initrd_list, FILE *out);

/* mapfile.c */
void *mapfile(int fd, size_t *len, bool writable);
void unmapfile(int fd, void *ptr, size_t len);

/* xmalloc.c */
void *xmalloc(size_t);
void *xcalloc(size_t, size_t);
int xasprintf(char **strp, const char *fmt, ...);

/* cwrite.c */
size_t c_fwrite(const void *ptr, size_t bytes, FILE *stream);
size_t c_writezero(size_t n, FILE *stream);

#endif
