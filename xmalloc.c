/*
 * xmalloc.c
 *
 * Simple error-checking version of malloc()
 *
 */

#include "wraplinux.h"

#include <errno.h>
#include <stdarg.h>
#include <string.h>

void *xmalloc(size_t size)
{
	void *p = malloc(size);

	if (!p) {
		fprintf(stderr, "%s: %s\n", program, strerror(errno));
		exit(EX_OSERR);
	}

	return p;
}

void *xcalloc(size_t nmemb, size_t size)
{
	void *p = calloc(nmemb, size);

	if (!p) {
		fprintf(stderr, "%s: %s\n", program, strerror(errno));
		exit(EX_OSERR);
	}

	return p;
}

int xasprintf(char **strp, const char *fmt, ...)
{
	va_list va;
	int n;

	va_start(va, fmt);
	n = vasprintf(strp, fmt, va);
	va_end(va);

	if (n < 0) {
		fprintf(stderr, "%s: %s\n", program, strerror(errno));
		exit(EX_OSERR);
	}

	return n;
}
